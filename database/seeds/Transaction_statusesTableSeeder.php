<?php

use Illuminate\Database\Seeder;

class Transaction_statusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('transaction_statuses')->insert([
	        	'name' => 'pending'
	        ]);
        DB::table('transaction_statuses')->insert([
	        	'name' => 'approved'
	        ]);
        DB::table('transaction_statuses')->insert([
	        	'name' => 'completed'
            ]);
        DB::table('transaction_statuses')->insert([
	        	'name' => 'rejected'
            ]);
            
        DB::table('transaction_statuses')->insert([
            'name' => 'cancelled'
        ]);
    }
}
