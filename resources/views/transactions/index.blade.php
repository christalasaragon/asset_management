@extends('layouts.app')

@section('content')

<div class="container">{{-- container --}} 
	
	{{-- start pending --}}
	<div class="row">
		<div class="col-md-8 mx-auto text-center">
		<h2>Pending</h2>
		</div>
		{{-- row --}}
		@foreach($pendings as $pending)
		<div class="col-md-8 mx-auto" id="accordion">
			{{-- col --}}
			
			<div class="card ">
				<div class="card-header" id="headingOne">
					<h5 class="mb-0">
						<button class="btn btn-link w-100" data-toggle="collapse" data-target="#transaction-{{$pending->id}}" aria-expanded="false" aria-controls="collapseOne">  {{$pending->user->name}} /{{$pending->reference_number}}/ {{ $pending->created_at->format('F d, Y ')}} 

					
							
							@if($pending->transaction_status->name == "pending")
							<span class="badge badge-warning float-right">{{$pending->transaction_status->name}}</span>
							@elseif($pending->transaction_status->name == "approved")
							<span class="badge badge-success float-right">{{$pending->transaction_status->name}}</span>
							@elseif($pending->transaction_status->name == "completed")
							<span class="badge badge-secondary float-right">{{$pending->transaction_status->name}}</span>
							@else
							<span class="badge badge-danger float-right">{{$pending->transaction_status_id->name}}</span>
							@endif

							
							@cannot('isAdmin')
							<form action="{{ route('transactions.update', $pending->id) }}" method="post">
									@csrf
									@method('put')
									<input type="hidden" name="transaction_status" id="transaction_status" value="5">
									<button class="btn btn-secondary w-10 float-right ">Cancel</button>
							</form>
							@endcannot
							
							
							
							@can('isAdmin')
							<form action="{{ route('transactions.update', $pending->id) }}" method="post">
								@csrf
								@method('put')
								<input type="hidden" name="transaction_status" id="transaction_status" value="4">
								<button class="btn btn-danger w-10 float-right mr-1 ">Reject</button>
							</form>

							<form action="{{ route('transactions.update', $pending->id) }}" method="post">
									@csrf
									@method('put')
									<input type="hidden" name="transaction_status" id="transaction_status" value="2">
									<button class="btn btn-success w-10 float-right mr-1">Approve</button>
							</form>
							@endcannot
							
						</button>
					</h5>
				</div>					
			</div>

			<div id="transaction-{{$pending->id}}" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
				<div class="card-body">
					{{-- start of card body --}}

						<h5 class="card-title text-center">Summary</h5>
						<div class="table-responsive mb-3">
						{{-- start of table --}}
							<table class="table table-sm table-borderless">
								<tbody>
									<tr>
										<td>Customer Name:</td>
										<td><strong>{{$pending->user->name}}</strong></td>
									</tr>

									<tr>
										<td>Reference Number:</td>
										<td><strong>{{strtoupper($pending->reference_number)}}</strong></td>
									</tr>

									<tr>
										<td>Borrow Date:</td>
										<td>{{$pending->borrow_date}}</td>

										<td>Return Date:</td>
										<td>{{$pending->return_date}}</td>


									</tr>

								</tbody>
								<tfoot class="text-center">
										<tr>
											<td colspan="2">
												<form action="{{route('transactions.show', $pending->id) }}" method="get">
													@csrf
													<button class="btn btn-primary">
														View Deetz
													</button>
												</form>
												
											</td>
										</tr>
									</tfoot>

								
							</table>
						</div>

						{{-- <h5 class="card-title text-center">Items</h5>
						<div class="table-responsive mb-3"> --}}
							{{-- start of table --}}
							{{-- <table class="table table-sm table-borderless text-center"> --}}
								{{-- <thead>
									<tr>
										<th>Item Name</th>
										<th>Description</th>
									</tr>

								</thead>
								<tbody>

							
								</tbody>

							</table>
						</div>  --}}
						 
				</div> 
				{{-- div ng card body --}}
			</div>
				{{-- div ng parent --}}
			</div>
			{{-- end col --}}
			@endforeach
	</div>
	{{-- end pending --}}

	{{-- start approved --}}
	<div class="row">
			<div class="col-md-8 mx-auto text-center">
				<hr>
			<h2>Approved</h2>
			</div>
			{{-- row --}}
			@foreach($approveds as $approved)
			<div class="col-md-8 mx-auto" id="accordion">
				{{-- col --}}
				
				<div class="card ">
					<div class="card-header" id="headingOne">
						<h5 class="mb-0">
							<button class="btn btn-link w-100" data-toggle="collapse" data-target="#transaction-{{$approved->id}}" aria-expanded="false" aria-controls="collapseOne"> {{$approved->user->name}} /	{{$approved->reference_number}} / {{ $approved->created_at->format('F d, Y ')}} 
	

							
							
								@if($approved->transaction_status->name == "pending")
								<span class="badge badge-warning float-right">{{$approved->transaction_status->name}}</span>
								@elseif($approved->transaction_status->name == "approved")
								<span class="badge badge-success float-right">{{$approved->transaction_status->name}}</span>
								@elseif($approved->transaction_status->name == "completed")
								<span class="badge badge-secondary float-right">{{$approved->transaction_status->name}}</span>
								@else
								<span class="badge badge-danger float-right">{{$approved->transaction_status_id->name}}</span>
								@endif
								
								@can('isAdmin')
								<form action="{{ route('transactions.update', $approved->id) }}" method="post">
										@csrf
										@method('put')
										<input type="hidden" name="transaction_status" id="transaction_status" value="3">
										<button class="btn btn-success w-10 float-right mr-1">Returned</button>
								</form>
								@endcan
							</button>
						</h5>
					</div>					
				</div>
	
				<div id="transaction-{{$approved->id}}" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
					<div class="card-body">
						{{-- start of card body --}}
	
							<h5 class="card-title text-center">Summary</h5>
							<div class="table-responsive mb-3">
							{{-- start of table --}}
								<table class="table table-sm table-borderless">
									<tbody>
										<tr>
											<td>Customer Name:</td>
											<td><strong>{{$approved->user->name}}</strong></td>
										</tr>
	
										<tr>
											<td>Reference Number:</td>
											<td><strong>{{strtoupper($approved->reference_number)}}</strong></td>
										</tr>
	
										<tr>
											<td>Borrow Date:</td>
											<td>{{$approved->borrow_date}}</td>
	
											<td>Return Date:</td>
											<td>{{$approved->return_date}}</td>
	
	
										</tr>
	
									</tbody>
									<tfoot class="text-center">
										<tr>
											<td colspan="2">
												<form action="{{route('transactions.show', $approved->id) }}" method="get">
													@csrf
													<button class="btn btn-primary">
														View Deetz
													</button>
												</form>
												
											</td>
										</tr>
									</tfoot>
								</table>
							</div>
					</div> 
					{{-- div ng card body --}}
				</div>
					{{-- div ng parent --}}
				</div>
				{{-- end col --}}
				@endforeach
	</div>{{-- end approved--}}

	{{-- start completed --}}
	<div class="row">
			<div class="col-md-8 mx-auto text-center">
				<hr>
			<h2>Log</h2>
			</div>
			{{-- row --}}
			@foreach($completeds as $completed)
			<div class="col-md-8 mx-auto" id="accordion">
				{{-- col --}}
				
				<div class="card ">
					<div class="card-header" id="headingOne">
						<h5 class="mb-0">
							<button class="btn btn-link w-100" data-toggle="collapse" data-target="#transaction-{{$completed->id}}" aria-expanded="false" aria-controls="collapseOne"> {{$completed->user->name}} /	{{$completed->reference_number}} / {{ $completed->created_at->format('F d, Y')}} 

							@if($completed->transaction_status->name == "pending")
							<span class="badge badge-warning float-right">{{$completed->transaction_status->name}}</span>
							@elseif($completed->transaction_status->name == "approved")
							<span class="badge badge-success float-right">{{$completed->transaction_status->name}}</span>
							@elseif($completed->transaction_status->name == "completed")
							<span class="badge badge-secondary float-right">{{$completed->transaction_status->name}}</span>
							@else
							<span class="badge badge-danger float-right">{{$completed->transaction_status->name}}</span>
							@endif

								
							</button>
						</h5>
					</div>					
				</div>
	
				<div id="transaction-{{$completed->id}}" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
					<div class="card-body">
						{{-- start of card body --}}
	
							<h5 class="card-title text-center">Summary</h5>
							<div class="table-responsive mb-3">
							{{-- start of table --}}
								<table class="table table-sm table-borderless">
									<tbody>
										<tr>
											<td>Customer Name:</td>
											<td><strong>{{$completed->user->name}}</strong></td>
										</tr>
	
										<tr>
											<td>Reference Number:</td>
											<td><strong>{{strtoupper($completed->reference_number)}}</strong></td>
										</tr>
	
										<tr>
											<td>Borrow Date:</td>
											<td>{{$completed->borrow_date}}</td>
	
											<td>Return Date:</td>
											<td>{{$completed->return_date}}</td>
	
	
										</tr>
	
									</tbody>
									<tfoot class="text-center">
										<tr>
											<td colspan="4">
												<form action="{{route('transactions.show', $completed->id) }}" method="get">
													@csrf
													<button class="btn btn-primary">
														View Deetz
													</button>
												</form>
												
											</td>
										</tr>
									</tfoot>
								</table>
							</div>
	

							 
					</div> 
					{{-- div ng card body --}}
				</div>
					{{-- div ng parent --}}
				</div>
				{{-- end col --}}
				@endforeach
	</div>{{-- end completed--}}



</div>{{-- end container --}}







@endsection

