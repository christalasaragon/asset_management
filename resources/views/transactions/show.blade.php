@extends('layouts.app')
@section('content')
	<div class="container">

		<div class="row">
			<div class="col-12 col-md-8 mx-auto">
				<h3 class="text-center">Transaction Details</h3>
				<hr>
			</div> {{-- end of col --}}
		</div>{{-- end of first row --}}

		<div class="row">
			<div class="col-12 col-md-8 mx-auto">
				<div class="table-responsive">
					<table class="table table-sm table-borderless">
						<tbody>
							<tr>
								<td>Customer Name:</td>
								<td><strong>{{$transaction->user->name}}</strong></td>
							</tr>

							<tr>
								<td>Transaction Number:</td>
								<td><strong>{{strtoupper($transaction->reference_number)}}</strong></td>
							</tr>

							<tr>
								<td>Status:</td>
								<td>{{$transaction->transaction_status->name}}</td>
							</tr>

							<tr>
								<td>Borrow Date:</td>
								<td>{{$transaction->borrow_date}}</td>
							</tr>	
							<tr>
								<td>Return Date:</td>
								<td>{{$transaction->return_date}}</td>
							</tr>														
						</tbody>
					</table>
					<table class="table">
						<thead>
							<th scope="col">Items</th>
							<th scope="col">Description</th>

						</thead>
						<tbody>
							@foreach($transaction->products as $transaction_product)
							<tr> {{-- per product --}}
								<td>{{$transaction_product->name}}</td>
								<td>{{$transaction_product->description}}</td>			
							</tr>
							@endforeach
						</tbody>
						
					</table>
				</div>
			</div>
		</div>{{-- end of second row --}}


	</div>{{-- end of container --}}

@endsection