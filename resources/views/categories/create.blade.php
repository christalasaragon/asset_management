@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-12 col-md-8 mx-auto">
				<h3 class="text-center">
					Edit Category
				</h3>
				<hr>
				@if(Session::has('update_failed'))
					<div class="alert alert-warning">
						{{Session::get('update_failed')}}
					</div>
				@endif
				@if(Session::has('update_success'))
					<div class="alert alert-success">
						{{Session::get('update_success')}}
					</div>
				@endif
				<form method="post" action="{{route('categories.update',['category' =>$category->id])}}" enctype="multipart/form-data">
					@csrf
					@method('put')
					{{-- input for name --}}
					<div class="form-group">
						<label for="name">Product name:</label>
						<input 
							type="text" 
							name="name" 
							class="form-control" 
							id="name"
							value="{{$category->name}}" 
							>
					<button class="btn btn-dark w-100">Edit Item</button>
				</form>
			</div>
		</div>
	</div>
@endsection