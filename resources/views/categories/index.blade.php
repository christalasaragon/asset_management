@extends('layouts.app')

@section('content')


<div class="container">
	<div class="row">
		<div class="col-12 col-md-10 mx-auto">
			<h3 class="text-center">
				Categories
			</h3>
			@if(Session::has('category_message'))
			<div class="alert alert-success">
				{{Session::get('category_message')}}
			</div>
			@endif
			{{-- validation --}}
			@if($errors->has('add-category'))
			<div class="alert alert-danger">
				<small class="mb-0">Category not added
				</small>
			</div>
			@endif
			@if(Session::has('destroy_message'))
			<div class="alert alert-success text-center">
				{{Session::get('destroy_message')}}
			</div>
			@endif

			<div class="row">
				<table class="table table-striped">

					<tbody>



						@foreach($categories as $category)
						<tr>

							<td><a href="" >{{$category->name}}</a></td>
							<td>

								{{-- delete button --}}
								<form action="{{route('categories.destroy',['category'=>$category->id])}}" method="post">
									@csrf
									@method('DELETE')
									<button class="btn btn-secondary w-10 my-1 float-right">Delete</button>
								</form>

								<a href="{{route('categories.edit',['category'=>$category->id])}}" 
									class="w-10 btn btn-primary my-1 mr-1 float-right">
									Edit
								</a>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>

				

				
			</div>
		</div>

		<div class="col-12 col-md-2 mx-auto">
			<h3 class="text-center">
				New Category
			</h3>
			<hr>
			{{-- alert message --}}


			<form method="post" action="{{route('categories.store')}}" enctype="multipart/form-data">
				@csrf
				<div class="form-group">
					<label for="add-category">
						Add category:	
					</label>
					<input type="text" name="add-category" id="add-category" class="form-control">
				</div>

				<button class="btn btn-outline-secondary w-100">Add category</button>

			</form>
		</div>
	</div>
</div>
@endsection

