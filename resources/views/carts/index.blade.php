@extends('layouts.app')

@section('content')


<div class="container">
	<div class="row">
		<div class="col-12">

			<h3 class="text-center">
				My Cart Details
			</h3>
		</div>
	</div>{{-- end of div row --}}
	<div class="row">
		<div class="col-12 col-md-8 mx-auto">

			<div class="table-responsive">
				<table class="table table-striped table-hover text-center">
					<thead>
						<tr>
							<th>Name</th>
							<th>Serial #</th>
							<th>Description</th>
							<th></th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						@if(Session::has('cart'))
						{{-- @foreach(Session) --}}
						{{-- {{dd(Session::get('cart'))}} --}}

						@foreach($products as $product)
						<tr>

							<th scope ="row">{{$product->name}}</th>

							<td>{{$product->serial_number}}</td>
							<td>{{$product->description}}</td>

							<td>
								<form action="{{route('carts.store')}}" method="post" class="add-to-cart-field">
									<div class="form-group">
										@csrf
										<input type="hidden" name="id" value="{{$product->id}}">

									</div>
									
								</form>
							</td>

							<td>


								<form action="{{route('carts.destroy',['cart' =>$product->id])}}" method="post">
									@csrf
									@method('DELETE')
									<button class="btn btn-danger">Remove</button>
								</form>
							</td>
						</tr>
						@endforeach

					</tbody>

				</table> {{-- end of table --}}
			</div> {{-- end of table responsive --}}

			<form action="{{route('transactions.store')}}" method="post" class="text-center">
				@csrf
				<div class="text-center my-3">
					
					<label for="borrow">Borrow Date:</label>
					<input type="date" name="borrow" class="form-control w-50 mx-auto">
					<label for="return">Return Date:</label>
					<input type="date" name="return" class="form-control w-50 mx-auto">

				</div>

				
				<button
				@if(!Auth::check())
				 data-toggle="modal" data-target="#authentication-modal" 
				@endif
				 class="btn btn-success w-25 mb-2">Checkout Cart</button>
				
			</form>

			<form action="{{route('carts.clear')}}" method="post" class="text-center">

				@csrf
				@method('DELETE')
				<button class="btn btn-outline-secondary w-25">Empty Cart</button>
			</form>
			@else
			<tr>
				<td colspan="6" class="text-center">There are no items in the cart.</td>
			</tr>

						{{-- <div class="alert alert-info"> Cart empty. Please visit our products using this <a href="{{}route('products.index')}">link</a>
						</div> --}}

						@endif


					</div>
				</div>
			</div>{{-- end of div container --}}
			{{-- javascript ni paypal --}}
			{{-- modal --}}
			<div class="modal fade" id="authentication-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLabel">Authentication is required</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							Please login to continue
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
							<a href="{{ route('login') }}" type="button" class="btn btn-primary">Login</a
							</div>
						</div>
					</div>
				</div>

				{{-- Render the Smart Payment Buttons --}}


				@endsection


