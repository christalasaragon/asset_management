@extends('layouts.app')

@section('content')



	{{-- @if(Session::has('destroy_message'))
		<div class="alert alert-success text-center">
			{{Session::get('destroy_message')}}
		</div>
		@endif --}}

		<div class="jumbotron" id="jumbo">
			<h1 id="tag" class="jumbotxt display-4 text-right">Golf Equipment Rental for the Travelling Golfer</h1>
			<p class="jumbotxt lead text-right"><em>Rent the latest golf equipments from leading golf brands</em></p>
		</div>

		<div class="container">

			<div class="row ">
				<div class="col-12 col-md-10 mx-auto">
					<div class="row">
						@foreach($products as $product)
						<div class="col-12 col-md-4 col-lg-3  mb-3">
							
							<div class="card">
								<div class="card-body text-center">
									<img src="{{url('/public/'.$product->image)}}" class="img-fluid card-img-top">
									<h2 class="card-title">
										{{$product->name}}
									</h2>

									<p class="card-text">
										{{$product->description}}
									</p>
									<p class="card-text">
										{{$product->status}}
									</p>
									@cannot('isAdmin')	
									<form action="{{route('carts.store')}}" method="post" class="add-to-cart-field text-center">
										@csrf
										<div class="form-group">
											<input type="hidden" name="id" value="{{$product->id}}">
										</div>
										<button	class="btn btn-success w-85">
											Add to Cart
										</button>
									</form>
									@endcannot

									@can('isAdmin')
									<a href="{{route('products.edit',['product'=>$product->id])}}" class="btn btn-warning btn-block mb-1 ">
										Edit 
									</a>

									{{-- delete button --}}
									<form action="{{route('products.destroy',$product->id)}}" method="post">
										@csrf
										@method('DELETE')
										<button class="btn btn-danger btn-block">Delete </button>
									</form>
									@endcan
								</div>
							</div>
						</div>
						@endforeach	
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection