@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-12 col-md-8 mx-auto">
				<h3 class="text-center">
					Add Golf Equipment
				</h3>
				<hr>
				<form method="post" action="{{route('products.store')}}" enctype="multipart/form-data">
					@csrf

					{{-- input for name --}}
					<div class="form-group">
						<label for="name">Golf Equipment name:</label>
						<input 
							type="text" 
							name="name" 
							class="form-control" 
							id="name"
							>
					</div>
						{{-- validation --}}
						@if($errors->has('name'))
							<div class="alert alert-danger">
								<small class="mb-0">Equipment name required</small>
							</div>
						@endif
					
					{{-- input for category --}}
					<div class="form-group">
					    <label for="category">Golf Equipment Category</label>
					    <select class="form-control" id="category" name="category">
					      @foreach($categories as $category)
					      	<option value="{{$category->id}}">{{$category->name}}</option>
					      @endforeach	
					    </select>
					</div>
						{{-- validation --}}
						@if($errors->has('category'))
							<div class="alert alert-danger">
								<small class="mb-0">Equipment category required</small>
							</div>
						@endif

					{{-- input for image --}}
					<div class="form-group">
						<label for="image">Equipment image:</label>
						<input 
							type="file" 
							name="image" 
							class="form-control-file" 
							id="image"
							>
					</div>		
						{{-- validation --}}
						@if($errors->has('image'))
							<div class="alert alert-danger">
								<small class="mb-0">Equipment image required. Check if image is not greater than 3mb.</small>
							</div>
						@endif			
					{{-- input for description --}}
					<div class="form-group">
						<label for="description">Equipment description:</label>
						<textarea 
							id="description" 
							class="form-control" 
							cols="30" 
							rows="10" 
							name="description"
						></textarea>
					</div>
						{{-- validation --}}
						@if($errors->has('description'))
							<div class="alert alert-danger">
								<small class="mb-0">Product description required</small>
							</div>
						@endif

					

					{{-- input for serial number --}}
					<div class="form-group">
						<label for="quantity">Serial Number:</label>
						<input 
							type="number"
							id="serial" 
							class="form-control" 
							name="serial"
						>
					</div>

					<button class="btn btn-dark w-100">Add Item</button>



				</form>
			</div> {{-- end of add product form --}}
			{{-- column for add category form --}}

			<div class="col-12 col-md-2 mx-auto">
				<h3 class="text-center">
					New Category
				</h3>
				<hr>
				{{-- alert message --}}
				@if(Session::has('category_message'))
				<div class="alert alert-success">
					{{Session::get('category_message')}}
				</div>
				@endif
				{{-- validation --}}
				@if($errors->has('add-category'))
					<div class="alert alert-danger">
						<small class="mb-0">Category not added
						</small>
					</div>
				@endif

				<form method="post" action="{{route('categories.store')}}" enctype="multipart/form-data">
					@csrf
					<div class="form-group">
						<label for="add-category">
						Add category:	
						</label>
						<input type="text" name="add-category" id="add-category" class="form-control">
					</div>

					<button class="btn btn-secondary w-100">Add category</button>
					
				</form>
			</div>
		</div>
	</div>
@endsection