@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-12 col-md-8 mx-auto">
				<h3 class="text-center">
					Edit Equipment
				</h3>
				<hr>
				@if(Session::has('update_failed'))
					<div class="alert alert-warning">
						{{Session::get('update_failed')}}
					</div>
				@endif
				@if(Session::has('update_success'))
					<div class="alert alert-success">
						{{Session::get('update_success')}}
					</div>
				@endif
				<form method="post" action="{{route('products.update',['product' =>$product->id])}}" enctype="multipart/form-data">
					@csrf
					@method('PUT')
					{{-- input for name --}}
					<div class="form-group">
						<label for="name">Equipment name:</label>
						<input 
							type="text" 
							name="name" 
							class="form-control" 
							id="name"
							value="{{$product->name}}" 
							>
					</div>
						{{-- validation --}}
						@if($errors->has('name'))
							<div class="alert alert-danger">
								<small class="mb-0">Product name required</small>
							</div>
						@endif

					{{-- input for category --}}
					<div class="form-group">
					    <label for="category">Product category</label>
					    <select class="form-control" id="category" name="category">
					      @foreach($categories as $category)
					      	<option 
					      		value="{{$category->id}} 
					      		"
					      		@if($product->category_id == $category->id) 
					      		selected
					      		@endif
					      		>
					      			{{$category->name}}
					      		</option>
					      @endforeach	
					    </select>
					</div>
						{{-- validation --}}
						@if($errors->has('category'))
							<div class="alert alert-danger">
								<small class="mb-0">Product category required</small>
							</div>
						@endif

					{{-- input for image --}}
					<div class="form-group">
						<label for="image">Product image:</label>
						<input 
							type="file" 
							name="image" 
							class="form-control-file" 
							id="image"
							>
					</div>		
						{{-- validation --}}
						@if($errors->has('image'))
							<div class="alert alert-danger">
								<small class="mb-0">Product image required.</small>
							</div>
						@endif		


					{{-- input for description --}}
					<div class="form-group">
						<label for="description">Product description:</label>
						<textarea 
							id="description" 
							class="form-control" 
							cols="30" 
							rows="10" 
							name="description"
						>{{$product->description}}</textarea>
					</div>
						{{-- validation --}}
						@if($errors->has('description'))
							<div class="alert alert-danger">
								<small class="mb-0">Product description required</small>
							</div>
						@endif

					
						{{-- validation --}}
						@if($errors->has('quantity'))
							<div class="alert alert-danger">
								<small class="mb-0">{{$errors->first('quantity')}}</small>
							</div>
						@endif

					{{-- input for serial number --}}
					<div class="form-group">
						<label for="price">Serial Number:</label>
						<input 
							type="number" 
							name="serial" 
							class="form-control" 
							id="serial"
							value="{{$product->serial_number}}" 
							>
					</div>
						{{-- validation --}}
						@if($errors->has('serial'))
							<div class="alert alert-danger">
								<small class="mb-0">{{$errors->first('serial')}}</small>
							</div>
						@endif
					
	

					<button class="btn btn-dark w-100">Edit Item</button>



				</form>
			</div>
		</div>
	</div>
@endsection