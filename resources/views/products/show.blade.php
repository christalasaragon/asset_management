@extends('layouts.app')
@section('content')
	<div class="container">
		<div class="row">
			<div class="col-12 col-md-8 mx-auto">
				<h3 class="text-center">
					View Golf Equipments
				</h3>
				<hr>
				<div class="card">
						<div class="card-body">
							<img src="{{url('/public/'.$product->image)}}" class="card-img-top">
							<h2 class="card-title">
								{{$product->name}}
							</h2>
							<p class="card-text">
								<strong>
									&#8369; {{$product->serial_number}}
								</strong>
							</p>
							<p class="card-text">
								{{$product->description}}
							</p>
							<p class="card-text">
								{{$product->quantity}}
							</p>
						</div>
						<div class="card-foot">
							{{-- add to cart button --}} 
							@cannot('isAdmin')
{{-- 							<a 
								href="#" 
								class="btn btn-success w-100 my-1"
								>
								Add to Cart
							</a> --}}
							<form action="{{route('carts.store')}}" method="post" class="add-to-cart-field">
									<div class="form-group">
										@csrf
										<input type="hidden" name="id" value="{{$product->id}}">
										<label for="quantity">Quantity:</label>
										{{-- start of append button --}}
										<div class="input-group mb-3">
											  <div class="input-group-prepend">
											    <button class="btn btn-outline-secondary deduct-quantity" type="button" data-id="{{$product->id}}">-</button>
											  </div>
											  
											  <input type="text" name="quantity" class="form-control input-quantity" data-id="{{$product->id}}">

											  <div class="input-group-append">
    											<button class="btn btn-outline-secondary add-quantity " type="button" data-id="{{$product->id}}">+</button>
  											  </div>
										</div>
										{{-- end of append button --}}
									</div>
								<button 
									
									type="submit" 
									class="btn btn-success w-100 my-1"
									>
									Add to Cart
								</button>
								</form>
							@endcannot
							@can('isAdmin')
							{{-- view button --}}
{{-- 							<a 
								href="{{route('products.show',['product' =>$product->id])}}" 
								class="btn btn-primary w-100 my-1"
								>
								View Product
							</a> --}}
							<a 
								href="{{route('products.index')}}" 
								class="btn btn-primary w-100 my-1"
								>
								Back to Products
							</a>
							{{-- edit button --}}
							<a 
								href="{{route('products.edit',['product'=>$product->id])}}" 
								class="w-100 btn btn-warning my-1"
								>
								Edit Product
							</a>
							{{-- delete button --}}
							<form action="{{route('products.destroy',['product'=>$product->id])}}" method="post">
								@csrf
								@method('DELETE')
								<button class="btn btn-danger w-100 my-1">Delete Product</button>
							</form>
							@endcan
						</div>
				</div>
			</div>
		</div>

	</div>
@endsection