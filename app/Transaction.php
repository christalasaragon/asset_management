<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Transaction extends Model
{
    use SoftDeletes;
	
    // public function items() {
    //     return $this->belongsToMany('App\Product', 'transaction_product')->withTimestamps();
    // }

    public function transaction_status (){
        return $this->belongsTo('App\Transaction_status');
    }
    public function user (){
        return $this->belongsTo('App\User');
    }

    public function products(){
        return $this->belongsToMany('App\Product','transaction_product')->withTimestamps();
    }
}
