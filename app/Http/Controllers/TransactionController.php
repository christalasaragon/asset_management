<?php

namespace App\Http\Controllers;

use App\Transaction;
use Illuminate\Http\Request;
use Auth;
use Str;
use Session;
use App\Product;
use App\Transaction_status;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();

        if(Auth::user()->id === 1){
            $pendings = Transaction::where('transaction_status_id', 1)->get();
            $approveds = Transaction::where('transaction_status_id',2)->get();
            $completeds = Transaction::whereIn('transaction_status_id',[3,4,5])->get();

        }else{
            $pendings = Transaction::where('user_id', Auth::user()->id)->where('transaction_status_id', 1)->get();
            $approveds = Transaction::where('user_id', Auth::user()->id)->where('transaction_status_id', 2)->get();
            $completeds = Transaction::where('user_id', Auth::user()->id)->whereIn('transaction_status_id', [3, 4,5])->get();
            

        }

        return view('transactions.index',
            [
                'pendings'=> $pendings,
                'approveds' => $approveds,
                'completeds' => $completeds

            ]
        );





        // $transactions = Transaction::all();
        // $products =Product::all();
        // $transaction_statuses = Transaction_status::all();
        // $transaction_products = Product::all()->where('user_id',);
        // if(Auth::user()->role_id === 1){
        //     $transactions = Transaction_status::all();
        // } else {
        //     $transactions = Transaction_status::where('user_id',Auth::user()->id)->get();
        // }
        
        // return view('transactions.index')
        // ->with('transactions',$transactions)
        // ->with('products',$products);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

            $transaction = new Transaction;
            $reference_number = Auth::user()->id . Str::random(10) . time();
            $user_id= Auth::user()->id;
            
            $borrow_date = $request->input('borrow');
            $return_date = $request->input('return');

            
            //status
            $transaction->reference_number = $reference_number;
            $transaction->user_id =$user_id;
            $transaction->borrow_date =$borrow_date;
            $transaction->return_date =$return_date;

            $transaction->save();
        
            $transaction_ids = array_keys(Session::get('cart'));
            
            $products = Product::find($transaction_ids);

            
            
            foreach (Session::get('cart') as $cart_order_id) {
                foreach ($products as $product) {
                    if($product->id == $cart_order_id){
                        $transaction->products()->attach($product->id);
                    }
                }
            }
            
            
           
            $transaction->save();
            Session::forget('cart');
            return redirect(route('transactions.show',['transaction'=>$transaction->id]));


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function show(Transaction $transaction)
    {
        // $this->authorize('view', $transaction);
        // echo "Showing single $transaction";
        return view('transactions.show')->with('transaction',$transaction);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function edit(Transaction $transaction)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Transaction $transaction)
    {
        
        $transaction->transaction_status_id = $request->input('transaction_status');
        $transaction->save();

        return redirect(route('transactions.index'))
            ->with('updated_transaction',$transaction->id);
    }

     
    public function destroy(Transaction $transaction)
    {
        $transaction->delete();
        return redirect(route('transactions.index'))->with('destroy_message','Transaction cancelled');  
    }
}
