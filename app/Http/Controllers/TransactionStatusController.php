<?php

namespace App\Http\Controllers;

use App\Transaction_status;
use Illuminate\Http\Request;

class TransactionStatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Transaction_status  $transaction_status
     * @return \Illuminate\Http\Response
     */
    public function show(Transaction_status $transaction_status)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Transaction_status  $transaction_status
     * @return \Illuminate\Http\Response
     */
    public function edit(Transaction_status $transaction_status)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Transaction_status  $transaction_status
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Transaction_status $transaction_status)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Transaction_status  $transaction_status
     * @return \Illuminate\Http\Response
     */
    public function destroy(Transaction_status $transaction_status)
    {
        //
    }
}
