<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use App\Category;
use Str;


class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();
        return view('products.index')->with('products',$products);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        
        $this->authorize('isAdmin');
        $categories = Category::all();
        return view('products.create')->with('categories',$categories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $this->authorize('create',$product);

        $request->validate([
                'name' => 'required|string',
                'category' => 'required|string|unique:categories,name',
                'image' => 'required|image', //3000kilobytes
                'description'=> 'required|string',
                'serial'=>'required|numeric'
            ]);

        // for image

        $file = $request->file('image');
        $file_name = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
        $file_extension = $file->extension();
        $random_chars = Str::random(10);
        $new_file_name = date('Y-m-d-H-i-s') . "_" . $random_chars . "_" . $file_name . "." . $file_extension;
        $filepath = $file->storeAs('images', $new_file_name, 'public');

        // get input value from store

        $name = $request->input('name');
        $category_id = $request->input('category');
        $description = $request->input('description');
        $serial_number = $request->input('serial');
        


        $product = new Product;
        $product->name = $name;
        $product->serial_number = $serial_number;
        $product->category_id = $category_id;
        $product->description = $description;
        $product->image = $filepath;
        $product->save();

        return redirect(route('products.index', ['product' => $product->id]));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        return view('products.show')->with('product',$product);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $this->authorize('update',$product);
        $categories = Category::all();
        return view('products.edit')
            ->with('product',$product)
            ->with('categories',$categories)
            ;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $request->validate([
                'name' => 'required|string',
                'category' => 'required|string|unique:categories,name',
                'image' => 'required|image', 
                'description'=> 'required|string',
                'serial'=>'required|numeric'
            ]);

        if(
            !$request->hasFile('image') &&
            $product->name == $request->input('name') &&
            $product->description == $request->input('description') &&
            $product->serial_number == $request->input('serial') &&
            $product->category_id == $request->input('category') 
        ){      
            $request->session()->flash('update_failed','Something went wrong');      
            
        } else {

            if($request->hasFile('image')){
                $request->validate([
                    'image' => 'required|image'
                ]);
            $file = $request->file('image'); 
            $file_name = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
            $file_extension = $file->extension();
            $random_chars = Str::random(10);
            $new_file_name = date('Y-m-d-H-i-s') . "_" . $random_chars . "_" . $file_name . "." . $file_extension;
            $filepath = $file->storeAs('images', $new_file_name, 'public');
            $product->image=$filepath;
            }

            $product->name = $request->input('name');
            $product->description = $request->input('description');
            $product->serial_number == $request->input('serial');
            $product->category_id = $request->input('category');
            $product->save();

            $request->session()->flash('update_success','Product is successfully updated');

        }   

        return redirect (route('products.edit',['product'=>$product->id]));


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        // $this->authorize('delete',$product);
        $product->delete();
        return redirect(route('products.index'))->with('destroy_message','Equipment removed');
    }

        
}
