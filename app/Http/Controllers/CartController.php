<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use Session;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $products = Product::all();
        $products = [];

        if(Session::has('cart')){
           
           $product_ids = array_keys(Session::get('cart'));
           $products = Product::find($product_ids);

          
            return view('carts.index')->with('products', $products);

        }

        return view('carts.index');



        // return view('carts.index')->with('products', $products);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         $request->validate([
            'id' => 'required',
            
        ]);
        
        $id = $request->input('id');
        
        return redirect(route('carts.index'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Product $product,Request $request)
    {
        
        
        $id = $request->input('id');

        // Session::push('cart.ids',$id);
        // dd(session::get('cart.ids',$id));

        
        $request->session()->put("cart.$id",$id);
        
        return redirect(route('carts.index'));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Session::forget("cart.$id");
        return redirect(route('carts.index'));
    }

    public function clearCart(){
        Session::forget("cart");
        return redirect(route('carts.index'));
    }


}
